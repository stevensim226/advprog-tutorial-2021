package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me (OK)

    @Override
    public String defend() {
        return "Defend with barrier (バリアー)";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
}
