package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me

    @Override
    public String defend() {
        return "Defend with armor (鎧)";
    }

    @Override
    public String getType() {
        return "Armor";
    }
}
