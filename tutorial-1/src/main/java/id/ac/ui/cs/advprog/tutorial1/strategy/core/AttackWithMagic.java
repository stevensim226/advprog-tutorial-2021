package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me (OK)

    @Override
    public String attack() {
        return "Attack with magic (魔法攻撃)";
    }

    @Override
    public String getType() {
        return "Magic";
    }
}
