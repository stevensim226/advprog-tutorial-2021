package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    //ToDo: Complete me (OK)

    @Override
    public String attack() {
        return "Attack with gun (銃撃)";
    }

    @Override
    public String getType() {
        return "Gun";
    }

}
