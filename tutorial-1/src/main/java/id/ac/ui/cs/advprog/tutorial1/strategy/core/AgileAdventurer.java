package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    //ToDo: Complete me (OK)

    /**
     * Constructor for AgileAdventurer.
     * defaults attack behavior to gun.
     * and defense behavior to barrier.
     */
    public AgileAdventurer() {
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "Agile";
    }
}
