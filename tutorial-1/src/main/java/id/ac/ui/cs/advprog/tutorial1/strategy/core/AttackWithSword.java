package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    //ToDo: Complete me (OK)

    @Override
    public String attack() {
        return "Attack with sword (剣攻撃)";
    }

    @Override
    public String getType() {
        return "Sword";
    }
}
