package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    //ToDo: Complete me (OK)

    /**
     * Constructor for MysticAdventurer.
     * defaults attack behavior to magic.
     * and defense behavior to shield.
     */
    public MysticAdventurer() {
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    @Override
    public String getAlias() {
        return "Mystic";
    }
}
