package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    //ToDo: Complete me (OK)

    @Override
    public String defend() {
        return "Defend with shield (盾)";
    }

    @Override
    public String getType() {
        return "Shield";
    }
}
