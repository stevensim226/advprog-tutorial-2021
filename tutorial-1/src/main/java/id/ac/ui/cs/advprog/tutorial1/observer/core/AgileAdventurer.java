package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;

        //ToDo: Complete Me (OK)
    }

    @Override
    public void update() {
        // Only Delivery and Rumble quests for AgileAdventurer
        switch (this.guild.getQuestType()) {
            case "D":
            case "R":
                this.getQuests().add(this.guild.getQuest());
                break;
            default:
                break;
        }
    }

    //ToDo: Complete Me (OK)
}
