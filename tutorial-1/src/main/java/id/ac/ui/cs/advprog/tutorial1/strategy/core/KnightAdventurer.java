package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    //ToDo: Complete me (OK)

    /**
     * Constructor for KnightAdventurer.
     * defaults attack behavior to sword.
     * and defense behavior to armor.
     */
    public KnightAdventurer() {
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    @Override
    public String getAlias() {
        return "Knight";
    }
}
