package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Override
    public List<Log> getAssistantLogs(String npm) {
        Mahasiswa assistant = mahasiswaRepository.findByNpm(npm);
        if (assistant == null || assistant.getMatkulAsisten() == null) {
            return null;
        }
        return assistant.getLogAsisten();
    }

    @Override
    public Log createLog(String npm, Log log) {
        Mahasiswa assistant = mahasiswaRepository.findByNpm(npm);
        if (assistant == null || assistant.getMatkulAsisten() == null) {
            return null;
        }
        log.setAsisten(assistant);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        Log existingLog = logRepository.findByIdLog(idLog);

        // Update only if log exists
        if (existingLog != null) {
            // Force take log's assistant and also use it on updated log.
            Mahasiswa assistant = existingLog.getAsisten();
            log.setIdLog(idLog);
            log.setAsisten(assistant);
            logRepository.save(log);
            return log;
        }
        return null;
    }

    @Override
    public void deleteLogByIdLog(int idLog) {
        Log existingLog = logRepository.findByIdLog(idLog);
        if (existingLog != null) {
            logRepository.delete(existingLog);
        }
    }

    @Override
    public List<LogReport> generateLogReport(String npm) {
        Mahasiswa asisten = mahasiswaRepository.findByNpm(npm);
        if (asisten != null) {
            List<Log> assistantLogs = mahasiswaRepository.findByNpm(npm).getLogAsisten();
            return LogReport.generateLogReport(assistantLogs);
        }
        return null;
    }
}
