package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;

import java.util.List;

public interface LogService {
    Log createLog(String npm, Log log);

    Log updateLog(int idLog, Log log);

    void deleteLogByIdLog(int idLog);

    List<Log> getAssistantLogs(String npm);

    List<LogReport> generateLogReport(String npm);
}
