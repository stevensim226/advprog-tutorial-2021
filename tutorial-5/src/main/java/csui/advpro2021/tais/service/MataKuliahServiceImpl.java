package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MataKuliahServiceImpl implements MataKuliahService {
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Override
    public Iterable<MataKuliah> getListMataKuliah() {
        return mataKuliahRepository.findAll();
    }

    @Override
    public MataKuliah getMataKuliah(String kodeMatkul) {
        return mataKuliahRepository.findByKodeMatkul(kodeMatkul);
    }

    @Override
    public MataKuliah createMataKuliah(MataKuliah mataKuliah) {
        if (mataKuliahRepository.findByKodeMatkul(mataKuliah.getKodeMatkul()) == null) {
            mataKuliahRepository.save(mataKuliah);
            return mataKuliah;
        }
        return null;
    }

    @Override
    public MataKuliah updateMataKuliah(String kodeMatkul, MataKuliah mataKuliah) {
        MataKuliah existingMatkul = mataKuliahRepository.findByKodeMatkul(kodeMatkul);

        // Check if matkul exists already
        if (existingMatkul != null) {
            mataKuliah.setKodeMatkul(kodeMatkul);
            mataKuliah.setTimAsisten(existingMatkul.getTimAsisten());
            mataKuliahRepository.save(mataKuliah);
            return mataKuliah;
        }
        return null;
    }

    @Override
    public void deleteMataKuliah(String kodeMatkul) {
        if (getMataKuliah(kodeMatkul) != null) {
            List<Mahasiswa> assistantList = getMataKuliah(kodeMatkul).getTimAsisten();
            if (assistantList == null || assistantList.size() == 0) {
                MataKuliah matkul = this.getMataKuliah(kodeMatkul);
                mataKuliahRepository.delete(matkul);
            }
        }
    }
}
