package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Assertions;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;

    private Log log;
    private Log existingLogOne;
    private Log existingLogTwo;


    private final String NON_EXISTENT_NPM = "1234298888";

    private final int ID_LOG_ONE = 1;
    private final int ID_LOG_TWO = 2;
    private final int ID_LOG_NEW = 3;
    private final int NON_EXISTENT_LOG_ID = 99;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906001111",
                "Anon Imus",
                "imus92@ui.ac.id",
                "3.80",
                "081200009999",
                new MataKuliah(), new LinkedList<Log>());

        existingLogOne = new Log(ID_LOG_ONE,
                Timestamp.valueOf("2021-01-26 09:00:00"),
                Timestamp.valueOf("2021-01-26 11:01:15"),
                "Periksa Kuis", mahasiswa);

        existingLogTwo = new Log(ID_LOG_TWO,
                Timestamp.valueOf("2021-01-26 15:00:00"),
                Timestamp.valueOf("2021-01-26 17:06:43"),
                "Asistensi", mahasiswa);

        mahasiswa.getLogAsisten().add(existingLogOne);
        mahasiswa.getLogAsisten().add(existingLogTwo);

        log = new Log(ID_LOG_NEW,
                Timestamp.valueOf("2021-01-27 14:00:00"),
                Timestamp.valueOf("2021-01-27 15:08:21"),
                "Asistensi", mahasiswa);
    }

    @Test
    public void testGetAssistantLogs() {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        List<Log> result = logService.getAssistantLogs(mahasiswa.getNpm());
        Assertions.assertEquals(result.size(), mahasiswa.getLogAsisten().size());
    }

    @Test
    public void testGetAssistantLogsToNonExistentNpmShouldFail() {
        lenient().when(mahasiswaRepository.findByNpm(NON_EXISTENT_NPM)).thenReturn(null);

        List<Log> result = logService.getAssistantLogs(NON_EXISTENT_NPM);
        Assertions.assertEquals(result, null);
    }

    @Test
    public void testNonTaGetLogsShouldFail() {
        mahasiswa.setMatkulAsisten(null);
        mahasiswa.setLogAsisten(null);

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        List<Log> result = logService.getAssistantLogs(mahasiswa.getNpm());
        Assertions.assertEquals(result, null);

    }

    @Test
    public void testCreateLogWithValidAssistantShouldSucceed() {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        Log result = logService.createLog(mahasiswa.getNpm(), log);
        Assertions.assertEquals(result.getIdLog(), log.getIdLog());
        verify(logRepository, times(1)).save(any());
    }

    @Test
    public void testCreateLogToNonTaShouldFail() {
        mahasiswa.setMatkulAsisten(null);
        mahasiswa.setLogAsisten(null);
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        Log result = logService.createLog(mahasiswa.getNpm(), log);
        Assertions.assertEquals(result, null);
        verify(logRepository, times(0)).save(any());
    }

    @Test
    public void testCreateLogToNonExistentTaShouldFail() {
        lenient().when(mahasiswaRepository.findByNpm(anyString())).thenReturn(null);

        Log result = logService.createLog(NON_EXISTENT_NPM, log);
        Assertions.assertEquals(result, null);
        verify(logRepository, times(0)).save(any());
    }

    @Test
    public void testUpdateLogWithValidDataShouldSucceed() {
        log.setDescription("Periksa UTS");
        log.setIdLog(ID_LOG_ONE);

        when(logRepository.findByIdLog(log.getIdLog())).thenReturn(existingLogOne);

        Log result = logService.updateLog(log.getIdLog(), log);
        verify(logRepository, times(1)).save(log);
        Assertions.assertEquals(result.getIdLog(), log.getIdLog());
    }

    @Test
    public void testUpdateLogWithNonExistentLog() {
        lenient().when(logRepository.findByIdLog(NON_EXISTENT_LOG_ID)).thenReturn(null);

        Log result = logService.updateLog(NON_EXISTENT_LOG_ID, log);
        verify(logRepository, times(0)).save(log);
        Assertions.assertEquals(result, null);
    }

    @Test
    public void testDeleteLogById() {
        when(logRepository.findByIdLog(ID_LOG_ONE)).thenReturn(existingLogOne);
        logService.deleteLogByIdLog(ID_LOG_ONE);

        verify(logRepository, times(1)).delete(existingLogOne);
    }

    @Test
    public void testDeleteNonExistentLogShouldFail() {
        lenient().when(logRepository.findByIdLog(anyInt())).thenReturn(null);
        logService.deleteLogByIdLog(NON_EXISTENT_LOG_ID);

        verify(logRepository, times(0)).delete(any());
    }

    @Test
    public void testGenerateLogReport() {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        List<LogReport> result = logService.generateLogReport(mahasiswa.getNpm());

        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void testGenerateLogReportNonExistentNpmShouldFail() {
        lenient().when(mahasiswaRepository.findByNpm(NON_EXISTENT_NPM)).thenReturn(null);

        List<LogReport> result = logService.generateLogReport(NON_EXISTENT_NPM);
        Assertions.assertEquals(result, null);

    }
}
