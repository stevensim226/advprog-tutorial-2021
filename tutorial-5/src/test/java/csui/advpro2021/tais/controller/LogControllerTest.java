package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogReport;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.util.LinkedList;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    private Mahasiswa mahasiswa;
    private Log existingLogOne;
    private Log existingLogTwo;
    private Log log;

    private int ID_LOG_ONE = 1;
    private int ID_LOG_TWO = 2;
    private  int ID_LOG_NEW = 3;

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906001111",
                "Anon Imus",
                "imus92@ui.ac.id",
                "3.80",
                "081200009999",
                new MataKuliah(), new LinkedList<Log>());

        existingLogOne = new Log(ID_LOG_ONE,
                Timestamp.valueOf("2021-01-26 09:00:00"),
                Timestamp.valueOf("2021-01-26 11:01:15"),
                "Periksa Kuis", mahasiswa);

        existingLogTwo = new Log(ID_LOG_TWO,
                Timestamp.valueOf("2021-01-26 15:00:00"),
                Timestamp.valueOf("2021-01-26 17:06:43"),
                "Asistensi", mahasiswa);

        mahasiswa.getLogAsisten().add(existingLogOne);
        mahasiswa.getLogAsisten().add(existingLogTwo);

        log = new Log(ID_LOG_NEW,
                Timestamp.valueOf("2021-01-27 14:00:00"),
                Timestamp.valueOf("2021-01-27 15:08:21"),
                "Asistensi", mahasiswa);
    }

    @Test
    public void testControllerGetLog() throws Exception {
        when(logService.getAssistantLogs(mahasiswa.getNpm())).thenReturn(mahasiswa.getLogAsisten());

        mvc.perform(get("/log/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value(ID_LOG_ONE));
    }

    @Test
    public void testControllerPostLog() throws Exception {
        when(logService.createLog(eq(mahasiswa.getNpm()), any())).thenReturn(log);

        mvc.perform(post("/log/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(log)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idLog").value(ID_LOG_NEW));
    }

    @Test
    public void testControllerUpdateLog() throws Exception {
        when(logService.updateLog(eq(ID_LOG_ONE), any(Log.class))).thenReturn(existingLogOne);

        mvc.perform(put("/log/" + existingLogOne.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(existingLogOne)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idLog").value(ID_LOG_ONE));
    }

    @Test
    public void testControllerDeleteLog() throws Exception {
        mvc.perform(delete("/log/" + existingLogOne.getIdLog()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerGetLogReport() throws Exception {
        when(logService
                .generateLogReport(mahasiswa.getNpm()))
                .thenReturn(LogReport
                        .generateLogReport(mahasiswa.getLogAsisten()));


        mvc.perform(get("/log/" + mahasiswa.getNpm() + "/log_report")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].month").value("January"))
                .andExpect(jsonPath("$[0].jamKerja").value(4))
                .andExpect(jsonPath("$[0].pembayaran").value(4*350));
    }
}
