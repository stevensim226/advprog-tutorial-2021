# Requirements yang ditemukan

### Mahasiswa
- Mahasiswa bisa mendaftar ke tepat 1 mata kuliah
- Pendaftaran asdos langsung diterima
- Mahasiswa yang sudah jadi asdos tidak bisa mendaftar lagi, dan tidak bisa dihapus
- Mahasiswa bisa diupdate data dirinya
- Mahasiswa (asdos) many to one ke Mata Kuliah

### Auth / bonus assignment
- Mahasiswa bisa login dengan NPM dan password
- Authentication dan Authorization diimplementasikan dengan JWT

### Mata Kuliah
- Mata kuliah bisa memiliki beberapa asdos
- Mata kuliah dengan asdos tidak boleh dihapus
- Mata kuliah bisa diupdate data mata kuliahnya
- Mata Kuliah one to many ke Mahasiswa (asdos)

### Log
- Mahasiswa yang menjadi asdos bisa membuat log
- Log bisa diupdate dan dihapus
- Ada fitur log summary yang merekap honor bulanan dalam satuan jam * 350 greil
- Log many to one ke Mahasiswa (asdos)

### Umum
- Terapkan data persistence dengan JPA dan PostgreSQL
- Semua data dikirim & dikembalikan dalam bentuk JSON
- Terapkan practice REST API