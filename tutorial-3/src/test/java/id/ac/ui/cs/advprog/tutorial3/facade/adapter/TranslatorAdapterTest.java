package id.ac.ui.cs.advprog.tutorial3.facade.adapter;

import id.ac.ui.cs.advprog.tutorial3.facade.core.adapter.TranslatorAdapter;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TranslatorAdapterTest {
    private final String ALPHA_STR = "HelloWorld";
    private final String RUNIC_STR = "KV^HnqcG%D";
    private TranslatorAdapter translatorAdapterInstance;
    private CodexTranslator codexTranslatorInstance;

    @BeforeEach
    public void setUp() {
        codexTranslatorInstance = new CodexTranslator();
        translatorAdapterInstance = new TranslatorAdapter(codexTranslatorInstance);
    }

    @Test
    public void testEncode() {
        Spell spellInstance = new Spell(ALPHA_STR, AlphaCodex.getInstance());
        String translatorResult = codexTranslatorInstance
                .translate(spellInstance, RunicCodex.getInstance())
                .getText();

        String adapterResult = translatorAdapterInstance
                .encode(spellInstance)
                .getText();

        assertEquals(translatorResult, adapterResult);
    }

    @Test
    public void testDecode() {
        Spell spellInstance = new Spell(RUNIC_STR, RunicCodex.getInstance());
        String translatorResult = codexTranslatorInstance
                .translate(spellInstance, AlphaCodex.getInstance())
                .getText();

        String adapterResult = translatorAdapterInstance
                .decode(spellInstance)
                .getText();

        assertEquals(translatorResult, adapterResult);
    }
}
