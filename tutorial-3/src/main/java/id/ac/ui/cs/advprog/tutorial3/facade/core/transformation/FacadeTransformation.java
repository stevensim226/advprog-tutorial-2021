package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.adapter.TranslatorAdapter;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Component
@Qualifier("FacadeTransformation")
public class FacadeTransformation implements Transformation {

    List<Transformation> transformationList = new LinkedList<Transformation>();

    /**
     * Default constructor w/ existing logic
     */
    public FacadeTransformation() {
        // Hard coded initialization for initial creation (for bonus)
        addTransformation(new CelestialTransformation());
        addTransformation(new AbyssalTransformation());
        addTransformation(new TranslatorAdapter(new CodexTranslator()));
        addTransformation(new CaesarTransformation());
    }

    @Override
    public Spell encode(Spell spell) {
        // No hardcode involved (for bonus)
        for (Transformation trans : transformationList) {
            spell = trans.encode(spell);
        }
        return spell;
    }

    @Override
    public Spell decode(Spell spell) {
        // No hardcode involved (for bonus)
        for (int ii = transformationList.size() - 1; ii >= 0; ii--) {
            spell = transformationList.get(ii).decode(spell);
        }
        return spell;
    }

    /**
     * Can be used to add more encryption logic. (for bonus)
     * @param transformation Transformation instance.
     */
    public void addTransformation(Transformation transformation) {
        this.transformationList.add(transformation);
    }
}
