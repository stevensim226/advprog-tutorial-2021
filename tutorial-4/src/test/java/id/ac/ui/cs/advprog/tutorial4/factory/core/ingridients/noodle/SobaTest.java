package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {
    private Soba sobaInstance;
    private String EXPECTED_DESC = "Adding Liyuan Soba Noodles...";

    @BeforeEach
    public void setup() throws Exception {
        sobaInstance = new Soba();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(sobaInstance.getDescription(), EXPECTED_DESC);
    }
}
