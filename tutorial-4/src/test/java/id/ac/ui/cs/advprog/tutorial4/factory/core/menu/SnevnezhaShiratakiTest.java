package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki snevnezhaShiratakiInstance;
    final private String SHIRATAKI_NAME = "Morepeko Flower Fish Shirataki";

    @BeforeEach
    public void setup() throws Exception {
        snevnezhaShiratakiInstance = new SnevnezhaShirataki(SHIRATAKI_NAME);
    }

    @Test
    public void testGetName() {
        assertEquals(snevnezhaShiratakiInstance.getName(), SHIRATAKI_NAME);
    }

    @Test
    public void testGetNoodleShouldReturnCorrectInstance() {
        assertTrue(snevnezhaShiratakiInstance.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testGetMeatShouldReturnCorrectInstance() {
        assertTrue(snevnezhaShiratakiInstance.getMeat() instanceof Fish);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        assertTrue(snevnezhaShiratakiInstance.getTopping() instanceof Flower);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        assertTrue(snevnezhaShiratakiInstance.getFlavor() instanceof Umami);
    }
}
