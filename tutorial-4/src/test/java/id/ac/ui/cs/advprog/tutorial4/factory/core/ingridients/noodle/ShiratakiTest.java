package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiratakiTest {
    private Shirataki shiratakiInstance;
    private String EXPECTED_DESC = "Adding Snevnezha Shirataki Noodles...";

    @BeforeEach
    public void setup() throws Exception {
        shiratakiInstance = new Shirataki();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(shiratakiInstance.getDescription(), EXPECTED_DESC);
    }
}
