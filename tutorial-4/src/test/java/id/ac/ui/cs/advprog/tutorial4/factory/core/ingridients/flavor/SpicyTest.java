package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpicyTest {
    private Spicy spicyInstance;
    private String EXPECTED_DESC = "Adding Liyuan Chili Powder...";

    @BeforeEach
    public void setup() throws Exception {
        spicyInstance = new Spicy();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(spicyInstance.getDescription(), EXPECTED_DESC);
    }
}
