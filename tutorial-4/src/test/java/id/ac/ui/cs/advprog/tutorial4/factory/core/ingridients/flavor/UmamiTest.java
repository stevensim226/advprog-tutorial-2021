package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UmamiTest {
    private Umami umamiInstance;
    private String EXPECTED_DESC = "Adding WanPlus Specialty MSG flavoring...";

    @BeforeEach
    public void setup() throws Exception {
        umamiInstance = new Umami();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(umamiInstance.getDescription(), EXPECTED_DESC);
    }
}
