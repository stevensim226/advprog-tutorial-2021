package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SweetTest {
    private Sweet sweetInstance;
    private String EXPECTED_DESC = "Adding a dash of Sweet Soy Sauce...";

    @BeforeEach
    public void setup() throws Exception {
        sweetInstance = new Sweet();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(sweetInstance.getDescription(), EXPECTED_DESC);
    }
}
