package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    private Cheese cheeseInstance;
    private String EXPECTED_DESC = "Adding Shredded Cheese Topping...";

    @BeforeEach
    public void setup() throws Exception {
        cheeseInstance = new Cheese();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(cheeseInstance.getDescription(), EXPECTED_DESC);
    }
}
