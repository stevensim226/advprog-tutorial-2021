package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PorkTest {
    private Pork porkInstance;
    private String EXPECTED_DESC = "Adding Tian Xu Pork Meat...";

    @BeforeEach
    public void setup() throws Exception {
        porkInstance = new Pork();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(porkInstance.getDescription(), EXPECTED_DESC);
    }
}
