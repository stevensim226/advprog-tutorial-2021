package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiIngredientFactoryTest {
    private SnevnezhaShiratakiIngredientFactory snevnezhaFactory = new SnevnezhaShiratakiIngredientFactory();

    @Test
    public void testGetNoodle() {
        assertTrue(snevnezhaFactory.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testGetMeat() {
        assertTrue(snevnezhaFactory.getMeat() instanceof Fish);
    }

    @Test
    public void testGetTopping() {
        assertTrue(snevnezhaFactory.getTopping() instanceof Flower);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(snevnezhaFactory.getFlavor() instanceof Umami);
    }
}
