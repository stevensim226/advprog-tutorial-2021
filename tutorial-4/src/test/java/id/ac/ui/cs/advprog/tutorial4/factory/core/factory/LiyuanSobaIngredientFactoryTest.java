package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaIngredientFactoryTest {
    private LiyuanSobaIngredientFactory liyuanFactory = new LiyuanSobaIngredientFactory();

    @Test
    public void testGetNoodle() {
        assertTrue(liyuanFactory.getNoodle() instanceof Soba);
    }

    @Test
    public void testGetMeat() {
        assertTrue(liyuanFactory.getMeat() instanceof Beef);
    }

    @Test
    public void testGetTopping() {
        assertTrue(liyuanFactory.getTopping() instanceof Mushroom);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(liyuanFactory.getFlavor() instanceof Sweet);
    }
}
