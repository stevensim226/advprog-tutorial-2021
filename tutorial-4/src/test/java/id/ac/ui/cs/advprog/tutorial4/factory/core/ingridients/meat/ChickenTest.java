package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {
    private Chicken chickenInstance;
    private String EXPECTED_DESC = "Adding Wintervale Chicken Meat...";

    @BeforeEach
    public void setup() throws Exception {
        chickenInstance = new Chicken();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(chickenInstance.getDescription(), EXPECTED_DESC);
    }
}
