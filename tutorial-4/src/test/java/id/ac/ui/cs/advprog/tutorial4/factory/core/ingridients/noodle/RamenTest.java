package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RamenTest {
    private Ramen ramenInstance;
    private String EXPECTED_DESC = "Adding Inuzuma Ramen Noodles...";

    @BeforeEach
    public void setup() throws Exception {
        ramenInstance = new Ramen();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(ramenInstance.getDescription(), EXPECTED_DESC);
    }
}
