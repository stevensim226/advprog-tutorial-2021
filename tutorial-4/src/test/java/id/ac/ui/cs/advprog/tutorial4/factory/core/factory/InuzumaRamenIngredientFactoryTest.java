package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenIngredientFactoryTest {
    private InuzumaRamenIngredientFactory inuzumaFactory = new InuzumaRamenIngredientFactory();

    @Test
    public void testGetNoodle() {
        assertTrue(inuzumaFactory.getNoodle() instanceof Ramen);
    }

    @Test
    public void testGetMeat() {
        assertTrue(inuzumaFactory.getMeat() instanceof Pork);
    }

    @Test
    public void testGetTopping() {
        assertTrue(inuzumaFactory.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(inuzumaFactory.getFlavor() instanceof Spicy);
    }

}
