package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {
    private Udon udonInstance;
    private String EXPECTED_DESC = "Adding Mondo Udon Noodles...";

    @BeforeEach
    public void setup() throws Exception {
        udonInstance = new Udon();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(udonInstance.getDescription(), EXPECTED_DESC);
    }
}
