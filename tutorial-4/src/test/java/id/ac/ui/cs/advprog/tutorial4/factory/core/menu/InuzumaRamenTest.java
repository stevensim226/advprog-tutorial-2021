package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenTest {
    private InuzumaRamen inuzumaRamenInstance;
    final private String RAMEN_NAME = "Bakufu Spicy Pork Ramen";

    @BeforeEach
    public void setup() throws Exception {
        inuzumaRamenInstance = new InuzumaRamen(RAMEN_NAME);
    }

    @Test
    public void testGetName() {
        assertEquals(inuzumaRamenInstance.getName(), RAMEN_NAME);
    }

    @Test
    public void testGetNoodleShouldReturnCorrectInstance() {
        assertTrue(inuzumaRamenInstance.getNoodle() instanceof Ramen);
    }

    @Test
    public void testGetMeatShouldReturnCorrectInstance() {
        assertTrue(inuzumaRamenInstance.getMeat() instanceof Pork);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        assertTrue(inuzumaRamenInstance.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        assertTrue(inuzumaRamenInstance.getFlavor() instanceof Spicy);
    }
}
