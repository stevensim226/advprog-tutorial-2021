package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonIngredientFactoryTest {
    private MondoUdonIngredientFactory mondoFactory = new MondoUdonIngredientFactory();

    @Test
    public void testGetNoodle() {
        assertTrue(mondoFactory.getNoodle() instanceof Udon);
    }

    @Test
    public void testGetMeat() {
        assertTrue(mondoFactory.getMeat() instanceof Chicken);
    }

    @Test
    public void testGetTopping() {
        assertTrue(mondoFactory.getTopping() instanceof Cheese);
    }

    @Test
    public void testGetFlavor() {
        assertTrue(mondoFactory.getFlavor() instanceof Salty);
    }
}
