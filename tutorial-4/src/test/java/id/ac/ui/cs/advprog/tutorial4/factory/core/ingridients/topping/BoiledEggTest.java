package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {
    private BoiledEgg boiledEggInstance;
    private String EXPECTED_DESC = "Adding Guahuan Boiled Egg Topping";

    @BeforeEach
    public void setup() throws Exception {
        boiledEggInstance = new BoiledEgg();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(boiledEggInstance.getDescription(), EXPECTED_DESC);
    }
}
