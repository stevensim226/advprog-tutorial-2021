package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaltyTest {
    private Salty saltyInstance;
    private String EXPECTED_DESC = "Adding a pinch of salt...";

    @BeforeEach
    public void setup() throws Exception {
        saltyInstance = new Salty();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(saltyInstance.getDescription(), EXPECTED_DESC);
    }
}
