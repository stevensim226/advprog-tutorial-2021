package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MushroomTest {
    private Mushroom mushroomInstance;
    private String EXPECTED_DESC = "Adding Shiitake Mushroom Topping...";

    @BeforeEach
    public void setup() throws Exception {
        mushroomInstance = new Mushroom();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(mushroomInstance.getDescription(), EXPECTED_DESC);
    }
}
