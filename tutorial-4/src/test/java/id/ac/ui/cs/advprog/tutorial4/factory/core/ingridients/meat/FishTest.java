package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    private Fish fishInstance;
    private String EXPECTED_DESC = "Adding Zhangyun Salmon Fish Meat...";

    @BeforeEach
    public void setup() throws Exception {
        fishInstance = new Fish();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(fishInstance.getDescription(), EXPECTED_DESC);
    }
}
