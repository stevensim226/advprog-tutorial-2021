package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    private LiyuanSoba liyuanSobaInstance;
    final private String SOBA_NAME = "WanPlus Beef Mushroom Soba";

    @BeforeEach
    public void setup() throws Exception {
        liyuanSobaInstance = new LiyuanSoba(SOBA_NAME);
    }

    @Test
    public void testGetName() {
        assertEquals(liyuanSobaInstance.getName(), SOBA_NAME);
    }

    @Test
    public void testGetNoodleShouldReturnCorrectInstance() {
        assertTrue(liyuanSobaInstance.getNoodle() instanceof Soba);
    }

    @Test
    public void testGetMeatShouldReturnCorrectInstance() {
        assertTrue(liyuanSobaInstance.getMeat() instanceof Beef);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        assertTrue(liyuanSobaInstance.getTopping() instanceof Mushroom);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        assertTrue(liyuanSobaInstance.getFlavor() instanceof Sweet);
    }
}
