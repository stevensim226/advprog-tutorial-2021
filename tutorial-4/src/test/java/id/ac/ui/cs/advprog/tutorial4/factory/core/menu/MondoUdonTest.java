package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    private MondoUdon mondoUdonInstance;
    final private String UDON_NAME = "Good Hunter Cheese Chicken Udon";

    @BeforeEach
    public void setup() throws Exception {
        mondoUdonInstance = new MondoUdon(UDON_NAME);
    }

    @Test
    public void testGetName() {
        assertEquals(mondoUdonInstance.getName(), UDON_NAME);
    }

    @Test
    public void testGetNoodleShouldReturnCorrectInstance() {
        assertTrue(mondoUdonInstance.getNoodle() instanceof Udon);
    }

    @Test
    public void testGetMeatShouldReturnCorrectInstance() {
        assertTrue(mondoUdonInstance.getMeat() instanceof Chicken);
    }

    @Test
    public void testGetToppingShouldReturnCorrectInstance() {
        assertTrue(mondoUdonInstance.getTopping() instanceof Cheese);
    }

    @Test
    public void testGetFlavorShouldReturnCorrectInstance() {
        assertTrue(mondoUdonInstance.getFlavor() instanceof Salty);
    }
}
