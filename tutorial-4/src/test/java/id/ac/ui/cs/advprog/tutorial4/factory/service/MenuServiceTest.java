package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MenuServiceTest {

    private MenuService menuService;
    private Class<?> menuServiceClass;

    @BeforeEach
    public void setup() throws Exception {
        menuService = new MenuServiceImpl();
        menuServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuService");
    }

    @Test
    public void testMenuServiceHasGetMenus() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();

        ParameterizedType pt = (ParameterizedType) getMenus.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Menu.class));
    }

    @Test
    public void testGetMenusShouldReturnCorrectAmount() {
        assertEquals(menuService.getMenus().size(), 4);
    }

    @Test
    public void testCreateMenuLiyuanSoba() {
        String menuName = "LS";
        Menu liyuanSoba = menuService.createMenu(menuName, "LiyuanSoba");
        assertTrue(liyuanSoba instanceof  LiyuanSoba);
        assertEquals(liyuanSoba.getName(), menuName);
    }

    @Test
    public void testCreateMenuInuzumaRamen() {
        String menuName = "IR";
        Menu inuzumaRamen = menuService.createMenu(menuName, "InuzumaRamen");
        assertTrue(inuzumaRamen instanceof InuzumaRamen);
        assertEquals(inuzumaRamen.getName(), menuName);
    }

    @Test
    public void testCreateMenuMondoUdon() {
        String menuName = "MU";
        Menu liyuanSoba = menuService.createMenu(menuName, "MondoUdon");
        assertTrue(liyuanSoba instanceof MondoUdon);
        assertEquals(liyuanSoba.getName(), menuName);
    }

    @Test
    public void testCreateMenuSnevnezhaShirataki() {
        String menuName = "SS";
        Menu liyuanSoba = menuService.createMenu(menuName, "SnevnezhaShirataki");
        assertTrue(liyuanSoba instanceof SnevnezhaShirataki);
        assertEquals(liyuanSoba.getName(), menuName);
    }
}
