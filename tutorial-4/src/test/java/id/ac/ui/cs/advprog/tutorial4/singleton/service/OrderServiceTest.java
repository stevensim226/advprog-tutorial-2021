package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderServiceTest {
    private final String DRINK_NAME = "Bottled Water";
    private final String FOOD_NAME = "Cup of Rice";

    private OrderService orderService = new OrderServiceImpl();

    @Test
    public void testOrderADrinkShouldUpdateDrink() {
        orderService.orderADrink(DRINK_NAME);
        assertEquals(DRINK_NAME, OrderDrink.getInstance().getDrink());
    }

    @Test
    public void testOrderAFoodShouldUpdateFood() {
        orderService.orderAFood(FOOD_NAME);
        assertEquals(FOOD_NAME, OrderFood.getInstance().getFood());
    }


}
