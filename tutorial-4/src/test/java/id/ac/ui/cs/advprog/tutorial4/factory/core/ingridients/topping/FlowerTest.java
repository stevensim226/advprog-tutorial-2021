package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlowerTest {
    private Flower flowerInstance;
    private String EXPECTED_DESC = "Adding Xinqin Flower Topping...";

    @BeforeEach
    public void setup() throws Exception {
        flowerInstance = new Flower();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(flowerInstance.getDescription(), EXPECTED_DESC);
    }
}
