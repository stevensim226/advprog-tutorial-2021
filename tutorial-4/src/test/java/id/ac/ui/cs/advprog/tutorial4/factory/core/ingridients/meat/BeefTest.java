package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeefTest {
    private Beef beefInstance;
    private String EXPECTED_DESC = "Adding Maro Beef Meat...";

    @BeforeEach
    public void setup() throws Exception {
        beefInstance = new Beef();
    }

    @Test
    public void testGetDescriptionReturnsExpectedDesc() {
        assertEquals(beefInstance.getDescription(), EXPECTED_DESC);
    }
}
