package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

/**
 * Creates ingredient instances for Mondo Udon
 */
public class MondoUdonIngredientFactory implements IngredientFactory {

    @Override
    public Noodle getNoodle() {
        return new Udon();
    }

    @Override
    public Meat getMeat() {
        return new Chicken();
    }

    @Override
    public Topping getTopping() {
        return new Cheese();
    }

    @Override
    public Flavor getFlavor() {
        return new Salty();
    }
}
