package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

/**
 * Creates ingredient instances for Liyuan Soba
 */
public class LiyuanSobaIngredientFactory implements IngredientFactory{

    @Override
    public Noodle getNoodle() {
        return new Soba();
    }

    @Override
    public Meat getMeat() {
        return new Beef();
    }

    @Override
    public Topping getTopping() {
        return new Mushroom();
    }

    @Override
    public Flavor getFlavor() {
        return new Sweet();
    }
}
