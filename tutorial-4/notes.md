### Lazy vs. Eager Instantiation

Mulai dari definisi pendek yaitu **Lazy Instantiation** berarti kita men-delay instantiasi objek Singleton hingga diperlukan
sedangkan untuk **Eager Instantiation** entah akan / tidak digunakan, akan selalu di-instantiasi di awal mula program berjalan.

Kelebihan dari **Lazy Instantiation** adalah kita bisa menyimpan memory space / instantiation time dari pembuatan 
objek apabila Singleton tersebut sifatnya berat.

Kekurangannya ada saat objek yang benar-benar digunakan setiap saat mulai dari awal jalannya
program bisa memperlama proses karena ada waktu tambahan untuk
"boot up" instance. Juga akan ada tambahan tugas untuk sinkronisasi saat adanya penggunaan
`getInstance()` di waktu sangat berdempetan dalam sistem yang multithreading,
intinya agar tidak terjadi pembuatan 2 buah instance

Sedangkan untuk kelebihan **Eager Instantiation** itu lebih pada kasus apabila objek tersebut memang digunakan
 di banyak case karena objek dibuat tepat setelah program dijalankan jadinya objek tersebut siap untuk
 digunakan kapanpun, sehingga bisa dibilang menyimpan waktu juga.
 
 Kekurangannya juga saat tidak digunakan
 dengan benar yaitu resource yang terbuang (bootup time, memory) apabila instance objek tersebut memang
 tidak diperlukan setiap saat.