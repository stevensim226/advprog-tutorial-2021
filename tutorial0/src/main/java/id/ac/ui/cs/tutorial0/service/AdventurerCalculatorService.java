package id.ac.ui.cs.tutorial0.service;

public interface AdventurerCalculatorService {
    /**
     * Count power potential from birth year based on service's logic
     * @param birthYear birthYear input
     * @return power potential counted from logic
     */
    public int countPowerPotensialFromBirthYear(int birthYear);

    /**
     * Get power class in String format from service's logic
     * @param power power counted from countPowerPotensialFromBirthYear
     * @return power class String
     */
    public String getPowerClass(int power);
}
