package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.FamiliarState;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;
    // TODO: Complete Me (OK)
    FamiliarSpell(Familiar familiar) {
        this.familiar = familiar;
    }

    @Override
    public void undo() {
        // Calls back the last spell
        // Automatically sets the current spell as last spell
        if (familiar.getPrevState() == FamiliarState.ACTIVE) {
            familiar.summon();
        } else {
            // TODO: Complete Me (OK)
            familiar.seal();
        }
    }
}
