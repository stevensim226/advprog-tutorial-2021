package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSealSpell extends FamiliarSpell {
    // TODO: Complete Me (OK)

    /**
     * Constructor for familiar seal spell.
     * @param familiar familiar that owns this spell.
     */
    public FamiliarSealSpell(Familiar familiar) {
        super(familiar);
    }

    @Override
    public void cast() {
        this.familiar.seal();
    }

    @Override
    public String spellName() {
        return familiar.getRace() + ":Seal";
    }
}
