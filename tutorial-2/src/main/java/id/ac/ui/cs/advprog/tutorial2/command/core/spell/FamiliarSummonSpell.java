package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSummonSpell extends FamiliarSpell {
	// TODO: Complete Me (OK)

    /**
     * Constructor for familiar summon spell.
     * @param familiar familiar that owns this spell.
     */
    public FamiliarSummonSpell(Familiar familiar) {
        super(familiar);
    }

    @Override
    public void cast() {
        this.familiar.summon();
    }

    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }
}
