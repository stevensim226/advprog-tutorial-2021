package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

    public HighSpiritSpell(HighSpirit spirit) {
        // TODO: Complete Me (OK)
        this.spirit = spirit;
    }

    @Override
    public void undo() {
        // Calls back the last spell
        // Automatically sets the current spell as last spell
        // TODO: Complete Me (OK)
        switch (spirit.getPrevState()) {
            case ATTACK:
                spirit.attackStance();
                break;
            case DEFEND:
                spirit.defenseStance();
                break;
            case STEALTH:
                spirit.stealthStance();
                break;
            case SEALED:
                spirit.seal();
                break;
        }
    }
}
